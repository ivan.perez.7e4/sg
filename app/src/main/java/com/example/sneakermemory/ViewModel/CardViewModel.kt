package com.example.sneakermemory.ViewModel

import android.annotation.SuppressLint
import android.os.SystemClock
import android.widget.Chronometer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sneakermemory.Model.Card

@SuppressLint("StaticFieldLeak")
class CardViewModel : ViewModel() {

    val moves = MutableLiveData<Int>()


    init {
        //chrono.start()
        moves.value = 0
        //timeDifference.value = (SystemClock.elapsedRealtime() - chrono.base)

    }

    fun incMoves() {
        this.moves.postValue(this.moves.value?.plus(1))
    }
}