package com.example.sneakermemory.Model

import android.media.Image

data class Card(val imageID: Int, var faceUp: Boolean = false, var matches: Boolean = false)
