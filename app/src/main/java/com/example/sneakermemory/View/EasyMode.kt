package com.example.sneakermemory.View

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.sneakermemory.Model.Card
import com.example.sneakermemory.R.drawable.*
import com.example.sneakermemory.ViewModel.CardViewModel
import com.example.sneakermemory.databinding.ActivityEasyModeBinding

//import com.example.sneakermemory.ViewModel.CardViewModel


class EasyMode : AppCompatActivity() {
    lateinit var imageButtons: List<ImageButton>
    lateinit var sneakerCards: List<Card>
    private var x: Int? = null
    var count = 0
    var score = 0
    var timeDifference = 0
    val mode = "Easy"
    val cardViewModel: CardViewModel by viewModels()
    var sec = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityEasyModeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val movements = binding.movement
        val pause = binding.pauseButton
        val pauseImageButton = binding.pauseImageButton
        val exit = binding.exit
        val cont = binding.cont
        var z = 0
        val chrono = binding.chrono
        val sneakerImages = mutableListOf(dunkhighspartangreen, j1ssb, dunkts)
        sneakerImages.addAll(sneakerImages)
        sneakerImages.shuffle()
        imageButtons = listOf(
            binding.card1,
            binding.card2,
            binding.card3,
            binding.card4,
            binding.card5,
            binding.card6
        )
        sneakerCards = imageButtons.indices.map { i -> Card(sneakerImages[i]) }

        cardViewModel.moves.observe(this, Observer {
            movements.text = "Movements: $it"
        })

        chrono.start()

        pause.setOnClickListener {
            chrono.stop()
            timeDifference = (SystemClock.elapsedRealtime() - chrono.base).toInt()
            pauseImageButton.visibility = View.VISIBLE
            exit.visibility = View.VISIBLE
            cont.visibility = View.VISIBLE
        }

        exit.setOnClickListener {
            val intent = Intent(this, SneakerMenu::class.java)
            startActivity(intent)
        }

        cont.setOnClickListener {
            chrono.start()
            chrono.base = (SystemClock.elapsedRealtime()) - timeDifference
            pauseImageButton.visibility = View.INVISIBLE
            exit.visibility = View.INVISIBLE
            cont.visibility = View.INVISIBLE
        }

        imageButtons.forEachIndexed { i, imageButton ->
            imageButton.setOnClickListener {
                sec = (SystemClock.elapsedRealtime() - chrono.base).toInt() / 1000
                z++
                if (z % 2 == 0)
                    cardViewModel.incMoves()

                cardViewModel.moves.observe(this, Observer {
                    score = ((100 - it) - sec)
                })
                ifMatches(i, score)
                updateSneakerImages(sneakerImages)
            }
        }
    }

    private fun ifMatches(i: Int, score: Int) {
        val sneakerCard = sneakerCards[i]
        if (sneakerCard.faceUp) {
            return
        }
        if (x == null) {
            sneakerImagesDown()
            x = i
        } else {
            isMatched(x!!, i, score)
            x = null
        }
        sneakerCard.faceUp = !sneakerCard.faceUp
    }

    private fun sneakerImagesDown() {
        for (sneakerCard in sneakerCards) {
            if (!sneakerCard.matches) {
                sneakerCard.faceUp = false
            }
        }
    }

    private fun isMatched(i1: Int, i2: Int, score: Int) {
        if (sneakerCards[i1].imageID == sneakerCards[i2].imageID) {
            sneakerCards[i1].matches = true; sneakerCards[i2].matches = true
            count += 2
        }

        if (count == imageButtons.size) {
            val intent = Intent(this, ScoreScreen::class.java)
            intent.putExtra("score", score)
            intent.putExtra("easy", mode)
            startActivity(intent)
        }
    }

    private fun updateSneakerImages(sneakerImages: MutableList<Int>) {
        sneakerCards.forEachIndexed { i, sneakerCard ->
            val imageButton = imageButtons[i]

            if (sneakerCard.faceUp) {
                imageButton.setImageResource(sneakerImages[i])
            } else {
                imageButton.setImageResource(newcarta)
            }
        }
    }

}