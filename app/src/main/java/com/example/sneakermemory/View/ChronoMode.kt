package com.example.sneakermemory.View

import android.annotation.SuppressLint
import android.content.Intent
import android.os.*
import android.view.View
import android.widget.Chronometer
import androidx.appcompat.app.AppCompatActivity
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.example.sneakermemory.Model.Card
import com.example.sneakermemory.R.drawable.*
import com.example.sneakermemory.databinding.ActivityChronoModeBinding


class ChronoMode : AppCompatActivity() {
    lateinit var imageButtons: List<ImageButton>
    lateinit var sneakerCards: List<Card>
    private var x: Int? = null
    var score = 0
    var mat = 0
    var count = 0
    var count2 = 0
    var timeDifference = 0
    val mode = "Chrono"
//    val cardViewModel: CardViewModel by viewModels()

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityChronoModeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val matches = binding.matches
        val pause = binding.pauseButton
        val pauseImageButton = binding.pauseImageButton
        val exit = binding.exit
        val cont = binding.cont
        val chrono = binding.chrono
        val sneakerImages = mutableListOf(dunkhighspartangreen, j1ssb, dunkts)
        sneakerImages.addAll(sneakerImages)
        sneakerImages.shuffle()
        imageButtons = listOf(
            binding.card1,
            binding.card2,
            binding.card3,
            binding.card4,
            binding.card5,
            binding.card6
        )
        sneakerCards = imageButtons.indices.map { i -> Card(sneakerImages[i]) }

        chrono.isCountDown = true
        chrono.base = SystemClock.elapsedRealtime() + 30000
        chrono.start()

        pause.setOnClickListener {
            chrono.stop()
            timeDifference = (SystemClock.elapsedRealtime() - chrono.base).toInt()
            pauseImageButton.visibility = View.VISIBLE
            exit.visibility = View.VISIBLE
            cont.visibility = View.VISIBLE
            //lowOpacity(parametros a opacar)
        }
        exit.setOnClickListener {
            val intent = Intent(this, SneakerMenu::class.java)
            startActivity(intent)
        }
        cont.setOnClickListener {
            chrono.start()
            chrono.base = (SystemClock.elapsedRealtime()) - timeDifference
            pauseImageButton.visibility = View.INVISIBLE
            exit.visibility = View.INVISIBLE
            cont.visibility = View.INVISIBLE
            //highOpacity(parametros a desopacar)
        }

        start2(matches, chrono, sneakerImages);

    }

    private fun start2(matches: TextView, chrono: Chronometer, sneakerImages: MutableList<Int>) {
        imageButtons.forEachIndexed { i, imageButton ->
            imageButton.setOnClickListener {
                if (((SystemClock.elapsedRealtime() - chrono.base).toInt() / 1000)>=0) {
                    val intent = Intent(this, ScoreScreen::class.java)
                    intent.putExtra("score", score)//Calcular score
                    startActivity(intent)
                }
                ifMatches(i, matches)
                sneakerCards[i].faceUp = true
                sinoseque(sneakerImages, count2)
                updateSneakerImages(sneakerImages)
            }
        }
    }

    private fun ifMatches(i: Int, matches: TextView) {
        val sneakerCard = sneakerCards[i]
        //Toast.makeText(this, "se queda aqui", Toast.LENGTH_SHORT).show()
        if (sneakerCard.faceUp) {
            return
        }
        if (x == null) {
            sneakerImagesDown()
            x = i
        } else {
            isMatched(x!!, i, matches)
            x = null
        }
        sneakerCard.faceUp = !sneakerCard.faceUp
    }

    private fun sneakerImagesDown() {
        for (sneakerCard in sneakerCards) {
            if (!sneakerCard.matches) {
                sneakerCard.faceUp = false
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun isMatched(i1: Int, i2: Int, matches: TextView) {
        if (sneakerCards[i1].imageID == sneakerCards[i2].imageID) {
            sneakerCards[i1].matches = true; sneakerCards[i2].matches = true
            count2 += 2
            mat += 2
            matches.text = "Matches: $mat"
            Toast.makeText(this, "$count2", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateSneakerImages(sneakerImages: MutableList<Int>) {
        sneakerCards.forEachIndexed { i, sneakerCard ->
            val imageButton = imageButtons[i]

            if (sneakerCard.faceUp) {
                imageButton.setImageResource(sneakerImages[i])
            } else {
                imageButton.setImageResource(newcarta)
            }
        }
    }
    private fun sinoseque(sneakerImages: MutableList<Int>, count2: Int) {
        if (count2==6){
            sneakerImagesDown2()
            sneakerImages.shuffle()
            this.count2 = 0
        }
    }
    private fun sneakerImagesDown2() {
        for (sneakerCard in sneakerCards) {
            if (sneakerCard.matches&&sneakerCard.faceUp) {
                sneakerCard.faceUp = false
                sneakerCard.matches = false
            }
        }
    }
}