package com.example.sneakermemory.View

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import com.example.sneakermemory.Model.Card
import com.example.sneakermemory.R.drawable.*
import com.example.sneakermemory.databinding.ActivityHardModeBinding

class HardMode : AppCompatActivity() {
    lateinit var imageButtons: List<ImageButton>
    lateinit var sneakerCards: List<Card>
    private var x: Int? = null
    var count = 0
    var score = 0
    var timeDifference = 0
    val mode = "Hard"
//    val cardViewModel: CardViewModel by viewModels()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityHardModeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val movements = binding.movement
        val pause = binding.pauseButton
        val pauseImageButton = binding.pauseImageButton
        val exit = binding.exit
        val cont = binding.cont
        var mov = 0
        var z = 0

        val chrono = binding.chrono
        val sneakerImages = mutableListOf(j1union, j4kaws, strangelove, j3amamaniere)
        sneakerImages.addAll(sneakerImages)
        sneakerImages.shuffle()
        imageButtons = listOf(
            binding.card1,
            binding.card2,
            binding.card3,
            binding.card4,
            binding.card5,
            binding.card6,
            binding.card7,
            binding.card8
        )
        sneakerCards = imageButtons.indices.map { i -> Card(sneakerImages[i]) }

        chrono.start()

        pause.setOnClickListener {
            chrono.stop()
            timeDifference = (SystemClock.elapsedRealtime() - chrono.base).toInt()
            pauseImageButton.visibility = View.VISIBLE
            exit.visibility = View.VISIBLE
            cont.visibility = View.VISIBLE
            //lowOpacity(parametros a opacar)
        }
        exit.setOnClickListener {
            val intent = Intent(this, SneakerMenu::class.java)
            startActivity(intent)
        }
        cont.setOnClickListener {
            chrono.start()
            chrono.base = (SystemClock.elapsedRealtime()) - timeDifference
            pauseImageButton.visibility = View.INVISIBLE
            exit.visibility = View.INVISIBLE
            cont.visibility = View.INVISIBLE
            //highOpacity(parametros a desopacar)
        }
        imageButtons.forEachIndexed { i, imageButton ->
            imageButton.setOnClickListener {
                val sec: Int = (SystemClock.elapsedRealtime() - chrono.base).toInt() / 1000
                z++
                if (z % 2 == 0)
                    mov++
                score = ((100 - mov) - sec)
                movements.text = "Movements: $mov"
                ifMatches(i, score)
                updateSneakerImages(sneakerImages)

            }
        }
    }

    private fun ifMatches(i: Int, score: Int) {
        val sneakerCard = sneakerCards[i]
        if (sneakerCard.faceUp) {
            return
        }
        if (x == null) {
            sneakerImagesDown()
            x = i
        } else {
            isMatched(x!!, i, score)
            x = null
        }
        sneakerCard.faceUp = !sneakerCard.faceUp
    }

    private fun sneakerImagesDown() {
        for (sneakerCard in sneakerCards) {
            if (!sneakerCard.matches) {
                sneakerCard.faceUp = false
            }
        }
    }

    private fun isMatched(i1: Int, i2: Int, score: Int) {
        if (sneakerCards[i1].imageID == sneakerCards[i2].imageID) {
            sneakerCards[i1].matches = true; sneakerCards[i2].matches = true
            count += 2
        }

        if (count == imageButtons.size) {
            val intent = Intent(this, ScoreScreen::class.java)
            intent.putExtra("score", score)
            intent.putExtra("hard", mode)
            startActivity(intent)
        }
    }

    private fun updateSneakerImages(sneakerImages: MutableList<Int>) {
        sneakerCards.forEachIndexed { i, sneakerCard ->
            val imageButton = imageButtons[i]

            if (sneakerCard.faceUp) {
                imageButton.setImageResource(sneakerImages[i])
            } else {
                imageButton.setImageResource(newcarta)
            }
        }
    }
}