package com.example.sneakermemory.View

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sneakermemory.databinding.ActivityScoreScreenBinding

class ScoreScreen : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityScoreScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val playAgainButton = binding.playAgain
        val menubutton = binding.menuButton
        val finalScore = binding.score
        val bundle: Bundle? = intent.extras
        val score: Int? = bundle?.getInt("score")
        val easy: String? = bundle?.getString("easy")
        val hard: String? = bundle?.getString("hard")
        val chrono: String? = bundle?.getString("chrono")
//        val lucky: String? = bundle?.getString("lucky")

        playAgainButton.setOnClickListener {
            if (easy.equals("Easy")) {
                val intent = Intent(this, EasyMode::class.java)
                startActivity(intent)
            }
            if (hard.equals("Hard")) {
                val intent = Intent(this, HardMode::class.java)
                startActivity(intent)
            }
            if (chrono.equals("Chrono")) {
                val intent = Intent(this, HardMode::class.java)
                startActivity(intent)
            }
//            if (lucky.equals("Lucky")) {
//                val intent = Intent(this, LuckyMode::class.java)
//                startActivity(intent)
//            }
        }
        menubutton.setOnClickListener {
            val intent = Intent(this, SneakerMenu::class.java)
            startActivity(intent)
        }


        finalScore.text = "$score points"
    }
}