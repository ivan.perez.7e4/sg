package com.example.sneakermemory.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.sneakermemory.R
import com.example.sneakermemory.databinding.ActivitySneakerMenuBinding

class SneakerMenu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySneakerMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)
        var optionDifficulty = binding.option
        val playButton = binding.playButton
        val helpButton = binding.helpButton
        val helpImageButton = binding.helpImageButton
        helpImageButton.visibility = View.INVISIBLE;
        val mainScreen = binding.mainScreen

        ArrayAdapter.createFromResource(
            this,
            R.array.optionDifficulty,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            optionDifficulty.adapter = adapter

        }

        playButton.setOnClickListener{
            if (optionDifficulty.selectedItem.equals("Easy")){
                val intent = Intent(this, EasyMode::class.java)
                startActivity(intent)
            }
            else if (optionDifficulty.selectedItem.equals("Hard Mode")){
                val intent = Intent(this, HardMode::class.java)
                startActivity(intent)
            }
            else if (optionDifficulty.selectedItem.equals("Chrono Mode")){
                val intent = Intent(this, ChronoMode::class.java)
                startActivity(intent)
            }
//            else if (optionDifficulty.selectedItem.equals("Lucky Mode")){
//                val intent = Intent(this, LuckyMode::class.java)
//                startActivity(intent)
//            }
        }
        helpButton.setOnClickListener{
            helpImageButton.visibility = View.VISIBLE
            lowOpacity(mainScreen, helpButton, playButton)

        }
        helpImageButton.setOnClickListener{
            helpImageButton.visibility = View.INVISIBLE
            highOpacity(mainScreen, helpButton, playButton)
        }
    }

    private fun lowOpacity(mainScreen: ConstraintLayout,helpButton: Button, playButton: Button) {
        mainScreen.background.alpha = 175
        helpButton.background.alpha = 175
        playButton.background.alpha = 175
    }
    private fun highOpacity(mainScreen: ConstraintLayout, helpButton: Button, playButton: Button) {
        mainScreen.background.alpha = 255
        helpButton.background.alpha = 255
        playButton.background.alpha = 255
    }
}


